<?php
/**
 * Шаблон для вывода на странице Bounties.
 *
 * Template name: Bounty
 *
 */

get_header(); ?>

<main class="main page-bounties">
    <div class="container">
        <div class="bounty__header-wrapper">
            <h2 class="hero__title">join your favourite crowdsale <span class="is-red">bounty compaign</span></h2>
            <div class="hero__subtitle">and be part of their success story</div>
        </div>
        <?php do_action( 'bounties' ); ?>
    </div>

</main><!-- #main -->

<?php
get_footer();
