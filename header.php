<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package storefront
 */

?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<header class="top-line">
		<div class="container">

			<?php
			/**
			 * Functions hooked into storefront_header action
			 *
			 * @hooked storefront_social_icons                     - 10
			 * @hooked site_branding                               - 20
			 * @hooked storefront_secondary_navigation             - 30
			 * @hooked storefront_primary_navigation               - 50
			 * @hooked storefront_header_cart                      - 60
			 */
			do_action( 'storefront_header' ); ?>

            <div class="fa-socials">
				<a class="fa-social-button" href="https://t.me/icochaserbountymanagement" target="_blank" rel="nofollow"><i class="fa fa-telegram" aria-hidden="true"></i></a>
                <a class="fa-social-button" href="https://twitter.com/Ico_Chaser" target="_blank" rel="nofollow"><i class="fa fa-twitter" aria-hidden="true"></i></a>
            </div>
		</div>

	</header><!-- #masthead -->

	<?php
	/**
	 * Functions hooked in to storefront_before_content
	 *
	 * @hooked storefront_header_widget_region - 10
	 */
	do_action( 'storefront_before_content' ); ?>
