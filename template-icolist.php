<?php
/**
 * The template for displaying the ICO list.
 *
 * Template name: ICO list
 *
 */

get_header(); ?>

<main class="main ico-list">

    <div class="container">

        <div class="ico-list__buttons">
            <div class="btn active" id="tab-products-all" onclick="showProducts('products-all')">All</div>
            <div class="btn" id="tab-products-featured" onclick="showProducts('products-featured')">Featured</div>
            <div class="btn" id="tab-products-ongoing" onclick="showProducts('products-ongoing')">Ongoing</div>
            <div class="btn" id="tab-products-comingsoon" onclick="showProducts('products-comingsoon')">Coming soon</div>
        </div>

        <div class="product u-header">
            <a class="woocommerce-LoopProduct-link">
                <div class="product__image-wrapper"></div>
                <h2 class="woocommerce-loop-product__title">ASSET</h2>
            </a>
            <div class="product__information">
                <div class="product__description-text">DESCRIPTION</div>
                <div class="product__information-row">
                    <div class="product__column u-no-resize u-left-column">
                        <div class="product__date-start">ICO OPENING</div>
                        <div class="product__date-end">ICO CLOSING</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php
    do_action( 'icolist' ); ?>

    <script>

        function showProducts(list) {

            var all = jQuery("#products-all");
            var featured = jQuery("#products-featured");
            var ongoing = jQuery("#products-ongoing");
            var comingsoon = jQuery("#products-comingsoon");

            all.removeClass("active");
            featured.removeClass("active");
            ongoing.removeClass("active");
            comingsoon.removeClass("active");

            jQuery("#tab-products-all").removeClass("active");
            jQuery("#tab-products-featured").removeClass("active");
            jQuery("#tab-products-ongoing").removeClass("active");
            jQuery("#tab-products-comingsoon").removeClass("active");

            jQuery("#"+list).addClass("active");
            jQuery("#tab-"+list).addClass("active");

        }

    </script>


</main><!-- #main -->

<?php
get_footer();
