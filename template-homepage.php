<?php
/**
 * Шаблон вывода для главной страницы - homepage.
 *
 *
 * Template name: Homepage
 *
 * @package storefront
 */

get_header(); ?>

<main class="main">

    <?php
    /**
     * Functions hooked in to homepage action
     *
     * @hooked homepage_content                - 10
     * @hooked homepage_featured_products      - 20
	 * @hooked limit_ongoing_products	       - 25
     * @hooked limit_comingsoon_products       - 30
     */
    do_action( 'homepage' ); ?>
    <div class="container">
        <?php echo do_shortcode('[medium_posts handle="@icochaser"]') ?>
    </div>
</main><!-- #main -->

<?php
get_footer();
