<?php
/**
 * Storefront template functions.
 *
 * @package storefront
 */

if ( ! function_exists( 'storefront_display_comments' ) ) {
	/**
	 * Storefront display comments
	 *
	 * @since  1.0.0
	 */
	function storefront_display_comments() {
		// If comments are open or we have at least one comment, load up the comment template.
		if ( comments_open() || '0' != get_comments_number() ) :
			comments_template();
		endif;
	}
}

if ( ! function_exists( 'storefront_comment' ) ) {
	/**
	 * Storefront comment template
	 *
	 * @param array $comment the comment array.
	 * @param array $args the comment args.
	 * @param int   $depth the comment depth.
	 * @since 1.0.0
	 */
	function storefront_comment( $comment, $args, $depth ) {
		if ( 'div' == $args['style'] ) {
			$tag = 'div';
			$add_below = 'comment';
		} else {
			$tag = 'li';
			$add_below = 'div-comment';
		}
		?>
		<<?php echo esc_attr( $tag ); ?> <?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ) ?> id="comment-<?php comment_ID() ?>">
		<div class="comment-body">
		<div class="comment-meta commentmetadata">
			<div class="comment-author vcard">
			<?php echo get_avatar( $comment, 128 ); ?>
			<?php printf( wp_kses_post( '<cite class="fn">%s</cite>', 'storefront' ), get_comment_author_link() ); ?>
			</div>
			<?php if ( '0' == $comment->comment_approved ) : ?>
				<em class="comment-awaiting-moderation"><?php esc_attr_e( 'Your comment is awaiting moderation.', 'storefront' ); ?></em>
				<br />
			<?php endif; ?>

			<a href="<?php echo esc_url( htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ); ?>" class="comment-date">
				<?php echo '<time datetime="' . get_comment_date( 'c' ) . '">' . get_comment_date() . '</time>'; ?>
			</a>
		</div>
		<?php if ( 'div' != $args['style'] ) : ?>
		<div id="div-comment-<?php comment_ID() ?>" class="comment-content">
		<?php endif; ?>
		<div class="comment-text">
		<?php comment_text(); ?>
		</div>
		<div class="reply">
		<?php comment_reply_link( array_merge( $args, array( 'add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
		<?php edit_comment_link( __( 'Edit', 'storefront' ), '  ', '' ); ?>
		</div>
		</div>
		<?php if ( 'div' != $args['style'] ) : ?>
		</div>
		<?php endif; ?>
	<?php
	}
}

if ( ! function_exists( 'storefront_footer_widgets' ) ) {
	/**
	 * Display the footer widget regions.
	 *
	 * @since  1.0.0
	 * @return void
	 */
	function storefront_footer_widgets() {
		$rows    = intval( apply_filters( 'storefront_footer_widget_rows', 1 ) );
		$regions = intval( apply_filters( 'storefront_footer_widget_columns', 4 ) );

		for ( $row = 1; $row <= $rows; $row++ ) :

			// Defines the number of active columns in this footer row.
			for ( $region = $regions; 0 < $region; $region-- ) {
				if ( is_active_sidebar( 'footer-' . strval( $region + $regions * ( $row - 1 ) ) ) ) {
					$columns = $region;
					break;
				}
			}

			if ( isset( $columns ) ) : ?>
				<div class=<?php echo '"footer-widgets row-' . strval( $row ) . ' col-' . strval( $columns ) . ' fix"'; ?>><?php

					for ( $column = 1; $column <= $columns; $column++ ) :
						$footer_n = $column + $regions * ( $row - 1 );

						if ( is_active_sidebar( 'footer-' . strval( $footer_n ) ) ) : ?>

							<div class="block footer-widget-<?php echo strval( $column ); ?>">
								<?php dynamic_sidebar( 'footer-' . strval( $footer_n ) ); ?>
							</div><?php

						endif;
					endfor; ?>

				</div><!-- .footer-widgets.row-<?php echo strval( $row ); ?> --><?php

				unset( $columns );
			endif;
		endfor;
	}
}

if ( ! function_exists( 'storefront_credit' ) ) {
	/**
	 * Display the theme credit
	 *
	 * @since  1.0.0
	 * @return void
	 */
	function storefront_credit() {
		?>
		<div class="site-info">
			<?php echo esc_html( apply_filters( 'storefront_copyright_text', $content = '&copy; ' . get_bloginfo( 'name' ) . ' ' . date( 'Y' ) ) ); ?>
			<?php if ( apply_filters( 'storefront_credit_link', true ) ) { ?>
			<br /> <?php printf( esc_attr__( '%1$s designed by %2$s.', 'storefront' ), 'Storefront', '<a href="http://www.woocommerce.com" title="WooCommerce - The Best eCommerce Platform for WordPress" rel="author">WooCommerce</a>' ); ?>
			<?php } ?>
		</div><!-- .site-info -->
		<?php
	}
}

if ( ! function_exists( 'storefront_header_widget_region' ) ) {
	/**
	 * Display header widget region
	 *
	 * @since  1.0.0
	 */
	function storefront_header_widget_region() {
		if ( is_active_sidebar( 'header-1' ) ) {
		?>
		<div class="header-widget-region" role="complementary">
			<div class="col-full">
				<?php dynamic_sidebar( 'header-1' ); ?>
			</div>
		</div>
		<?php
		}
	}
}

if ( ! function_exists( 'site_branding' ) ) {
	/**
	 * Site branding wrapper and display
	 *
	 * @since  1.0.0
	 * @return void
	 */
	function site_branding() {
		?>
		<div class="top-line__logo-wrapper">
			<?php site_title_or_logo(); ?>
		</div>
		<?php
	}
}

if ( ! function_exists( 'site_title_or_logo' ) ) {
	/**
	 * Display the site title or logo
	 *
	 * @since 2.1.0
	 * @param bool $echo Echo the string or return it.
	 * @return string
	 */
	function site_title_or_logo( $echo = true ) {
		if ( function_exists( 'the_custom_logo' ) && has_custom_logo() ) {
			$logo = get_custom_logo();
			$html = is_home() ? '<h1 class="logo">' . $logo . '</h1>' : $logo;
		} elseif ( function_exists( 'jetpack_has_site_logo' ) && jetpack_has_site_logo() ) {
			// Copied from jetpack_the_site_logo() function.
			$logo    = site_logo()->logo;
			$logo_id = get_theme_mod( 'custom_logo' ); // Check for WP 4.5 Site Logo
			$logo_id = $logo_id ? $logo_id : $logo['id']; // Use WP Core logo if present, otherwise use Jetpack's.
			$size    = site_logo()->theme_size();
			$html    = sprintf( '<a class="site-logo-link" href="%1$s" >%2$s</a>',
				esc_url( home_url( '/' ) ),
				wp_get_attachment_image(
					$logo_id,
					$size,
					false,
					array(
						'class'     => 'site-logo attachment-' . $size,
						'data-size' => $size,
						'itemprop'  => 'logo'
					)
				)
			);

			$html = apply_filters( 'jetpack_the_site_logo', $html, $logo, $size );
		} else {
			$tag = is_home() ? 'h1' : 'div';

			$html = '<' . esc_attr( $tag ) . ' class="beta site-title"><a href="' . esc_url( home_url( '/' ) ) . '" rel="home">' . esc_html( get_bloginfo( 'name' ) ) . '</a></' . esc_attr( $tag ) .'>';


		}

		if ( ! $echo ) {
			return $html;
		}

		echo $html;
	}
}

if ( ! function_exists( 'primary_navigation' ) ) {
	/**
	 * Display Primary Navigation
	 *
	 * @since  1.0.0
	 * @return void
	 */
	function primary_navigation() {
		?>
		<nav class="main-navigation">
			<?php
			wp_nav_menu(
				array(
					'theme_location'	=> 'primary',
					'container_class'	=> 'primary-navigation',
					)
			);
			?>
		</nav><!-- #site-navigation -->
		<?php
	}
}

if ( ! function_exists( 'storefront_secondary_navigation' ) ) {
	/**
	 * Display Secondary Navigation
	 *
	 * @since  1.0.0
	 * @return void
	 */
	function storefront_secondary_navigation() {
	    if ( has_nav_menu( 'secondary' ) ) {
		    ?>
		    <nav class="secondary-navigation" role="navigation" aria-label="<?php esc_html_e( 'Secondary Navigation', 'storefront' ); ?>">
			    <?php
				    wp_nav_menu(
					    array(
						    'theme_location'	=> 'secondary',
						    'fallback_cb'		=> '',
					    )
				    );
			    ?>
		    </nav><!-- #site-navigation -->
		    <?php
		}
	}
}

if ( ! function_exists( 'storefront_homepage_header' ) ) {
	/**
	 * Display the page header without the featured image
	 *
	 * @since 1.0.0
	 */
	function storefront_homepage_header() {
		edit_post_link( __( 'Edit this section', 'storefront' ), '', '', '', 'button storefront-hero__button-edit' );
		?>
        <div class="container">
            <header class="entry-header">
                <?php
                the_title( '<h1 class="entry-title">', '</h1>' );
                ?>
            </header><!-- .entry-header -->
        </div>
		<?php
	}
}

if ( ! function_exists( 'storefront_page_header' ) ) {
	/**
	 * Display the page header
	 *
	 * @since 1.0.0
	 */
	function storefront_page_header() {
		?>
		<div class="container">
			<header class="entry-header">
				<?php
				storefront_post_thumbnail( 'full' );
				the_title( '<h1 class="entry-title">', '</h1>' );
				?>
			</header><!-- .entry-header -->
		</div>
		<?php
	}
}

if ( ! function_exists( 'storefront_page_content' ) ) {
	/**
	 * Display the post content
	 *
	 * @since 1.0.0
	 */
	function storefront_page_content() {
		?>
        <div class="container">
            <div class="entry-content">
                <?php the_content(); ?>
                <?php
                    wp_link_pages( array(
                        'before' => '<div class="page-links">' . __( 'Pages:', 'storefront' ),
                        'after'  => '</div>',
                    ) );
                ?>
            </div><!-- .entry-content -->
        </div>
		<?php
	}
}

if ( ! function_exists( 'storefront_post_header' ) ) {
	/**
	 * Display the post header with a link to the single post
	 *
	 * @since 1.0.0
	 */
	function storefront_post_header() {
		?>
		<header class="entry-header">
		<?php
		if ( is_single() ) {
			storefront_posted_on();
			the_title( '<h1 class="entry-title">', '</h1>' );
		} else {
			if ( 'post' == get_post_type() ) {
				storefront_posted_on();
			}

			the_title( sprintf( '<h2 class="alpha entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
		}
		?>
		</header><!-- .entry-header -->
		<?php
	}
}

if ( ! function_exists( 'storefront_post_content' ) ) {
	/**
	 * Display the post content with a link to the single post
	 *
	 * @since 1.0.0
	 */
	function storefront_post_content() {
		?>
		<div class="entry-content">
		<?php

		/**
		 * Functions hooked in to storefront_post_content_before action.
		 *
		 * @hooked storefront_post_thumbnail - 10
		 */
		do_action( 'storefront_post_content_before' );

		the_content(
			sprintf(
				__( 'Continue reading %s', 'storefront' ),
				'<span class="screen-reader-text">' . get_the_title() . '</span>'
			)
		);

		do_action( 'storefront_post_content_after' );

		wp_link_pages( array(
			'before' => '<div class="page-links">' . __( 'Pages:', 'storefront' ),
			'after'  => '</div>',
		) );
		?>
		</div><!-- .entry-content -->
		<?php
	}
}

if ( ! function_exists( 'storefront_post_meta' ) ) {
	/**
	 * Display the post meta
	 *
	 * @since 1.0.0
	 */
	function storefront_post_meta() {
		?>
		<aside class="entry-meta">
			<?php if ( 'post' == get_post_type() ) : // Hide category and tag text for pages on Search.

			?>
			<div class="author">
				<?php
					echo get_avatar( get_the_author_meta( 'ID' ), 128 );
					echo '<div class="label">' . esc_attr( __( 'Written by', 'storefront' ) ) . '</div>';
					the_author_posts_link();
				?>
			</div>
			<?php
			/* translators: used between list items, there is a space after the comma */
			$categories_list = get_the_category_list( __( ', ', 'storefront' ) );

			if ( $categories_list ) : ?>
				<div class="cat-links">
					<?php
					echo '<div class="label">' . esc_attr( __( 'Posted in', 'storefront' ) ) . '</div>';
					echo wp_kses_post( $categories_list );
					?>
				</div>
			<?php endif; // End if categories. ?>

			<?php
			/* translators: used between list items, there is a space after the comma */
			$tags_list = get_the_tag_list( '', __( ', ', 'storefront' ) );

			if ( $tags_list ) : ?>
				<div class="tags-links">
					<?php
					echo '<div class="label">' . esc_attr( __( 'Tagged', 'storefront' ) ) . '</div>';
					echo wp_kses_post( $tags_list );
					?>
				</div>
			<?php endif; // End if $tags_list. ?>

		<?php endif; // End if 'post' == get_post_type(). ?>

			<?php if ( ! post_password_required() && ( comments_open() || '0' != get_comments_number() ) ) : ?>
				<div class="comments-link">
					<?php echo '<div class="label">' . esc_attr( __( 'Comments', 'storefront' ) ) . '</div>'; ?>
					<span class="comments-link"><?php comments_popup_link( __( 'Leave a comment', 'storefront' ), __( '1 Comment', 'storefront' ), __( '% Comments', 'storefront' ) ); ?></span>
				</div>
			<?php endif; ?>
		</aside>
		<?php
	}
}

if ( ! function_exists( 'storefront_paging_nav' ) ) {
	/**
	 * Display navigation to next/previous set of posts when applicable.
	 */
	function storefront_paging_nav() {
		global $wp_query;

		$args = array(
			'type' 	    => 'list',
			'next_text' => _x( 'Next', 'Next post', 'storefront' ),
			'prev_text' => _x( 'Previous', 'Previous post', 'storefront' ),
			);

		the_posts_pagination( $args );
	}
}

if ( ! function_exists( 'storefront_post_nav' ) ) {
	/**
	 * Display navigation to next/previous post when applicable.
	 */
	function storefront_post_nav() {
		$args = array(
			'next_text' => '%title',
			'prev_text' => '%title',
			);
		the_post_navigation( $args );
	}
}

if ( ! function_exists( 'storefront_posted_on' ) ) {
	/**
	 * Prints HTML with meta information for the current post-date/time and author.
	 */
	function storefront_posted_on() {
		$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
		if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
			$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time> <time class="updated" datetime="%3$s">%4$s</time>';
		}

		$time_string = sprintf( $time_string,
			esc_attr( get_the_date( 'c' ) ),
			esc_html( get_the_date() ),
			esc_attr( get_the_modified_date( 'c' ) ),
			esc_html( get_the_modified_date() )
		);

		$posted_on = sprintf(
			_x( 'Posted on %s', 'post date', 'storefront' ),
			'<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
		);

		echo wp_kses( apply_filters( 'storefront_single_post_posted_on_html', '<span class="posted-on">' . $posted_on . '</span>', $posted_on ), array(
			'span' => array(
				'class'  => array(),
			),
			'a'    => array(
				'href'  => array(),
				'title' => array(),
				'rel'   => array(),
			),
			'time' => array(
				'datetime' => array(),
				'class'    => array(),
			),
		) );
	}
}

if ( ! function_exists( 'storefront_product_categories' ) ) {
	/**
	 * Display Product Categories
	 * Hooked into the `homepage` action in the homepage template
	 *
	 * @since  1.0.0
	 * @param array $args the product section args.
	 * @return void
	 */
	function storefront_product_categories( $args ) {

		if ( storefront_is_woocommerce_activated() ) {

			$args = apply_filters( 'storefront_product_categories_args', array(
				'limit' 			=> 3,
				'columns' 			=> 3,
				'child_categories' 	=> 0,
				'orderby' 			=> 'name',
				'title'				=> __( 'Shop by Category', 'storefront' ),
			) );

			$shortcode_content = storefront_do_shortcode( 'product_categories', apply_filters( 'storefront_product_categories_shortcode_args', array(
				'number'  => intval( $args['limit'] ),
				'columns' => intval( $args['columns'] ),
				'orderby' => esc_attr( $args['orderby'] ),
				'parent'  => esc_attr( $args['child_categories'] ),
			) ) );

			/**
			 * Only display the section if the shortcode returns product categories
			 */
			if ( false !== strpos( $shortcode_content, 'product-category' ) ) {

				echo '<section class="storefront-product-section storefront-product-categories" aria-label="' . esc_attr__( 'Product Categories', 'storefront' ) . '">';

				do_action( 'storefront_homepage_before_product_categories' );

				echo '<h2 class="section-title">' . wp_kses_post( $args['title'] ) . '</h2>';

				do_action( 'storefront_homepage_after_product_categories_title' );

				echo $shortcode_content;

				do_action( 'storefront_homepage_after_product_categories' );

				echo '</section>';

			}
		}
	}
}

if ( ! function_exists( 'storefront_recent_products' ) ) {
	/**
	 * Display Recent Products
	 * Hooked into the `homepage` action in the homepage template
	 *
	 * @since  1.0.0
	 * @param array $args the product section args.
	 * @return void
	 */
	function storefront_recent_products( $args ) {

		if ( storefront_is_woocommerce_activated() ) {

			$args = apply_filters( 'storefront_recent_products_args', array(
				'columns' 			=> 4,
				'title'				=> 'Coming Soon',
				'category'			=> 'coming-soon'
			) );

			$shortcode_content = storefront_do_shortcode( 'recent_products', apply_filters( 'storefront_recent_products_shortcode_args', array(
				'columns'  => intval( $args['columns'] ), 'category' => $args['category'],
			) ) );

			/**
			 * Only display the section if the shortcode returns products
			 */
			if ( false !== strpos( $shortcode_content, 'product' ) ) {

				echo '<section class="recent-products u-cooming-soon" aria-label="' . esc_attr__( 'Recent Products', 'storefront' ) . '">';

				do_action( 'storefront_homepage_before_recent_products' );

				?>
                <div class="container">
                <div class="featured-products__top-line">
                <?php

				echo '<h2 class="featured-products__title">' . $args['title'] . '</h2>';

				?>
                <a class="btn" href="<?php echo( esc_url( home_url( '/' ) ) ); ?>ico-list">SEE MORE</a>
                </div>
                <?php

				do_action( 'storefront_homepage_after_recent_products_title' );

				echo $shortcode_content;

				do_action( 'storefront_homepage_after_recent_products' );

				echo '</div></section>';

			}
		}
	}
}


/************************
 * Для главной страницы *
 * -------------------- *
 ************************/

/**
 * Вывод продуктов (максимум 8)
 *
 * @param $title - заголовок
 * @param $category - id категории
 */
function getMainPageProducts( $title, $category ) {

	if ( storefront_is_woocommerce_activated() ) {

		$args = apply_filters( 'storefront_recent_products_args', array(
			'limit'				=> 8,
			'title'				=> $title,
			'category'			=> $category
		) );

		$shortcode_content = storefront_do_shortcode('recent_products', apply_filters( 'storefront_recent_products_shortcode_args',
			array(
				'per_page' => intval( $args['limit'] ),
				'category' => $args['category']
			)
		) );

		/**
		 * Вывод только если есть продукты
		 */
		if ( false !== strpos( $shortcode_content, 'product' ) ) {
			?>

			<section class="recent-products u-ongoing" aria-label="<?php esc_attr__( 'Recent Products', 'storefront' ) ?>">
				<div class="container">
					<div class="featured-products__top-line">
						<h3 class="featured-products__title"><?php echo $args['title'] ?></h3>
						<a class="btn" href="<?php echo( esc_url( home_url( '/' ) ) ); ?>ico-list">SEE MORE</a>
					</div>
					<?php echo $shortcode_content ?>

				</div>
			</section>

			<?php
		}
	}
}

/*
 * Вывод списка постов с сайта medium.com
 *
 * используется плагин Display Medium Posts (https://github.com/acekyd/display-medium-posts)
 */
function medium_posts_display($atts){
    ob_start();
     $a = shortcode_atts(array('handle'=>'-1', 'default_image'=>'//i.imgur.com/p4juyuT.png', 'display' => 3, 'offset' => 0, 'total' => 3, 'list' => false, 'publication' => false), $atts);
    // No ID value
    if(strcmp($a['handle'], '-1') == 0){
            return "";
    }
    $handle=$a['handle'];
    $default_image = $a['default_image'];
    $display = $a['display'];
    $offset = $a['offset'];
    $total = $a['total'];
    $list = $a['list'] =='false' ? false: $a['list'];
    $publication = $a['publication'] =='false' ? false: $a['publication'];

    $data = file_get_contents("https://medium.com/".$handle."/latest?format=json");
    $data = str_replace("])}while(1);</x>", "", $data);
    if($publication) {
        //If handle provided is specified as a publication
        $json = json_decode($data);
        $items = array();
        $count = 0;
        if(isset($json->payload->posts))
        {
            $posts = $json->payload->posts;
            foreach($posts as $post)
            {
                $items[$count]['title'] = $post->title;
                $items[$count]['url'] = 'https://medium.com/'.$handle.'/'.$post->uniqueSlug;
                $items[$count]['subtitle'] = isset($post->virtuals->subtitle) ? $post->virtuals->subtitle : "";
                if(!empty($post->virtuals->previewImage->imageId))
                {
                    $image = '//cdn-images-1.medium.com/max/500/'.$post->virtuals->previewImage->imageId;
                }
                else {
                    $image = $default_image;
                }
                $items[$count]['image'] = $image;
                $items[$count]['duration'] = round($post->virtuals->readingTime);
                $items[$count]['date'] = isset($post->firstPublishedAt) ? date('Y.m.d', $post->firstPublishedAt/1000): "";

                $count++;
            }
            if($offset)
            {
                $items = array_slice($items, $offset);
            }

            if(count($items) > $total)
            {
                $items = array_slice($items, 0, $total);
            }
        }
    }
    else {

        $json = json_decode($data);
        $items = array();
        $count = 0;
        if(isset($json->payload->references->Post))
        {
            $posts = $json->payload->references->Post;
            foreach($posts as $post)
            {
                $items[$count]['title'] = $post->title;
                $items[$count]['url'] = 'https://medium.com/'.$handle.'/'.$post->uniqueSlug;
                $items[$count]['subtitle'] = isset($post->content->subtitle) ? $post->content->subtitle : "";
                if(!empty($post->virtuals->previewImage->imageId))
                {
                    $image = '//cdn-images-1.medium.com/max/500/'.$post->virtuals->previewImage->imageId;
                }
                else {
                    $image = $default_image;
                }
                $items[$count]['image'] = $image;
                $items[$count]['duration'] = round($post->virtuals->readingTime);
                $items[$count]['date'] = isset($post->firstPublishedAt) ? date('Y.m.d', $post->firstPublishedAt/1000): "";

                $count++;
            }
            if($offset)
            {
                $items = array_slice($items, $offset);
            }

            if(count($items) > $total)
            {
                $items = array_slice($items, 0, $total);
            }
        }
    }
    ?>
    <section class="medium-posts">
        <h3 class="title is-3">Latest Posts</h3>
        <?php foreach($items as $item) { ?>
        <article class="card-post">
            <header class="card-post__header">
                <a href="<?php echo $item['url']; ?>" target="_blank">
                    <img class="card-post__image" src="<?php echo $item['image']; ?>">
                    <h4 class="title is-4"><?php echo $item['title']; ?></h4>
                </a>
            </header>
            <div class="card-post__body">
                <div class="card-post__text"><?php echo $item['subtitle']; ?></div>
            </div>
            <div class="card-post__button-wrapper">
                <a class="button" href="<?php echo $item['url']; ?>" target="_blank">Read</a>
            </div>
        </article>

        <?php } ?>
    </section>
    <?php
        if(empty($items)) echo "<div class='display-medium-no-post'>No posts found!</div>";
    ?>
    <?php
    return ob_get_clean();
}
add_shortcode('medium_posts', 'medium_posts_display');

/**
 * Вывод продуктов ONGOING
 *
 * @since  1.0.0
 * @param array $args the product section args.
 * @return void
 */
function limit_ongoing_products( $args ) {
	getMainPageProducts('Ongoing', 'ongoing');
}

/**
 * Вывод продуктов COOMING SOON
 *
 * @since  1.0.0
 * @param array $args the product section args.
 * @return void
 */
function limit_comingsoon_products( $args ) {
	getMainPageProducts('Comming Soon', 'coming-soon');
}


/**
 * Вывод продуктов, карусель для главной страницы
 *
 * @since  1.0.0
 * @param array $args the product section args.
 * @return void
 */
function homepage_featured_products( $args ) {

	if ( storefront_is_woocommerce_activated() ) {

		$args = apply_filters( 'storefront_recent_products_args', array(
			'orderby'  => 'date',
			'order'    => 'desc',
			'title'    => 'Featured',
			'category' => 'featured'
		) );

		$shortcode_content = storefront_do_shortcode(
			'recent_products',
			apply_filters(
				'storefront_recent_products_shortcode_args',
				array(
					'orderby'  => esc_attr( $args['orderby'] ),
					'order'    => esc_attr( $args['order'] ),
					'category' => $args['category']
				)
			)
		);

		/**
		 * Вывод только если есть продукты
		 */
		if ( false !== strpos( $shortcode_content, 'product' ) ) {
			?>

			<section class="featured-products" aria-label="<?php esc_attr__( 'Recent Products', 'storefront' ) ?>">
				<div class="container">
					<div class="featured-products__top-line">
						<h3 class="featured-products__title"><?php echo $args['title'] ?></h3>
						<a class="btn" href="<?php echo( esc_url( home_url( '/' ) ) ); ?>ico-list">SEE MORE</a>
					</div>
					<?php echo $shortcode_content ?>

				</div>
			</section>

			<?php
		}
	}
}

/* ^^^^^^^^^^^^^^^^^^^^^^^^^^^ *
 * КОНЕЦ. Для главной страницы *
 * --------------------------- *
 *******************************/


/*************************
 * Для страницы Bounties *
 * --------------------- *
 *************************/

/**
 * Вывод Bounties HOT
 *
 * @since  1.0.0
 * @param array $args the product section args.
 * @return void
 */
function bounties_hot_products( $args ) {
	if ( storefront_is_woocommerce_activated() ) {

		$args = apply_filters( 'storefront_recent_products_args', array(
            'limit'    => -1,
			'title'    => 'Bounties',
			'category' => 'bounties-hot'
		) );

		$shortcode_content = storefront_do_shortcode(
			'recent_products',
			apply_filters(
				'storefront_recent_products_shortcode_args',
				array(
				    'per_page' => intval( $args['limit'] ),
					'category' => $args['category']
				)
			)
		);

		/**
		 * Вывод только если есть продукты
		 */
		if ( false !== strpos( $shortcode_content, 'product' ) ) { ?>

			<section class="bounties u-hot">
				<div class="container">

					<div class="bounties__body">
					    <?php echo $shortcode_content ?>
					</div>

					<script>
					jQuery(document).ready(function(){
                        jQuery('.bounties.u-hot .products').owlCarousel({
                            loop:true,
                            margin:10,
                            nav:false,
                            autoplay: true,
                            autoplayTimeout: 7000,
                            responsive:{
                                0:{
                                    items:1
                                },
                                570:{
                                    items:2
                                },
                                900:{
                                    items:3
                                },
                                1200:{
                                    items:4
                                }
                            }
                        });
                    });
                    </script>

				</div>

			</section><?php

		}
	}
}

/**
 * Вывод Bounties
 *
 * @since  1.0.0
 * @param array $args the product section args.
 * @return void
 */
function bounties_products( $args ) {
	if ( storefront_is_woocommerce_activated() ) {

		$args = apply_filters( 'storefront_recent_products_args', array(
		    'limit'    => -1,
			'category' => 'bounties'
		) );

		$shortcode_content = storefront_do_shortcode(
			'recent_products',
			apply_filters(
				'storefront_recent_products_shortcode_args',
				array(
				    'per_page' => intval( $args['limit'] ),
					'category' => $args['category']
				)
			)
		);

		/**
		 * Вывод только если есть продукты
		 */
		if ( false !== strpos( $shortcode_content, 'product' ) ) { ?>

			<section class="bounties">
				<div class="container">
					<h3 class="bounties__title"><?php echo $args['title'] ?></h3>

					<div class="bounties__body">
					    <?php echo $shortcode_content ?>
					</div>

				</div>
			</section><?php

		}
	}
}

/* ^^^^^^^^^^^^^^^^^^^^^^^^^^^^ *
 * КОНЕЦ. Для страницы Bounties *
 * ---------------------------- *
 *******************************/

if ( ! function_exists( 'tefit_ongoing_products' ) ) {
	/**
	 * Вывод продуктов из категории ongoing
	 * Hooked into the `homepage` action in the homepage template
	 *
	 * @since  1.0.0
	 * @param array $args the product section args.
	 * @return void
	 */
	function tefit_ongoing_products( $args ) {

		if ( storefront_is_woocommerce_activated() ) {

			$args = apply_filters( 'storefront_recent_products_args', array(
				'columns' 			=> 4,
				'title'				=> 'Ongoing',
				'category'			=> 'ongoing',

			) );

			$shortcode_content = storefront_do_shortcode( 'recent_products', apply_filters( 'storefront_recent_products_shortcode_args', array(
				'columns'  => intval( $args['columns'] ), 'category' => $args['category']
			) ) );

			/**
			 * Only display the section if the shortcode returns products
			 */
			if ( false !== strpos( $shortcode_content, 'product' ) ) {

				echo '<section class="recent-products u-ongoing" aria-label="' . esc_attr__( 'Recent Products', 'storefront' ) . '">';

				do_action( 'storefront_homepage_before_recent_products' );

				?>
                <div class="container">
                <div class="featured-products__top-line">
                <?php

				echo '<h2 class="featured-products__title">' . $args['title'] . '</h2>';

				?>
                <a class="btn" href="<?php echo( esc_url( home_url( '/' ) ) ); ?>ico-list">SEE MORE</a>
                </div>
                <?php

				do_action( 'storefront_homepage_after_recent_products_title' );

				echo $shortcode_content;

				do_action( 'storefront_homepage_after_recent_products' );

				echo '</div></section>';

			}
		}
	}
}

if ( ! function_exists( 'featured_product_new' ) ) {

	function featured_product_new( $args ) {
		if ( storefront_is_woocommerce_activated() ) {

		    $args = apply_filters( 'storefront_recent_products_args', array(
		        'limit'             => -1,
				'columns' 			=> 4,

			) );

			?>

			<section id="products-all" class="active">
                <div class="container">
                    <?php
                        $shortcode_content = storefront_do_shortcode( 'recent_products', apply_filters( 'storefront_recent_products_shortcode_args', array(
                            'per_page' => intval( $args['limit'] ),
                            'category' => 'featured'
                        ) ) );
                        if ( false !== strpos( $shortcode_content, 'product' ) ) {
                            echo $shortcode_content;
                        }

                        $shortcode_content = storefront_do_shortcode( 'recent_products', apply_filters( 'storefront_recent_products_shortcode_args', array(
                            'per_page' => intval( $args['limit'] ),
                            'category' => 'ongoing'
                        ) ) );
                        if ( false !== strpos( $shortcode_content, 'product' ) ) {
                            echo $shortcode_content;
                        }

                        $shortcode_content = storefront_do_shortcode( 'recent_products', apply_filters( 'storefront_recent_products_shortcode_args', array(
                            'per_page' => intval( $args['limit'] ),
                            'category' => 'coming-soon'
                        ) ) );
                        if ( false !== strpos( $shortcode_content, 'product' ) ) {
                            echo $shortcode_content;
                        }
			        ?>
                </div>
            </section><?php
		}
	}
}

if ( ! function_exists( 'ongoing_products' ) ) {

	function ongoing_products( $args ) {
		if ( storefront_is_woocommerce_activated() ) {

		    $args = apply_filters( 'storefront_recent_products_args', array(
				'limit'             => -1,
				'category'			=> 'ongoing'
			) );

			$shortcode_content = storefront_do_shortcode( 'recent_products', apply_filters( 'storefront_recent_products_shortcode_args', array(
				'per_page' => intval( $args['limit'] ),
				'category' => $args['category'],
			) ) );

			?>

			<section id="products-ongoing">
                <div class="container">
                    <?php
                        if ( false !== strpos( $shortcode_content, 'product' ) ) {
                            echo $shortcode_content;
                        } else {
                            echo 'no results';
                        }
			        ?>
                </div>
            </section><?php
		}
	}
}

if ( ! function_exists( 'comingsoon_products' ) ) {

	function comingsoon_products( $args ) {
		if ( storefront_is_woocommerce_activated() ) {

		    $args = apply_filters( 'storefront_recent_products_args', array(
		        'limit'             => -1,
				'category'			=> 'coming-soon'
			) );

			$shortcode_content = storefront_do_shortcode( 'recent_products', apply_filters( 'storefront_recent_products_shortcode_args', array(
				'per_page' => intval( $args['limit'] ),
				'category' => $args['category'],
			) ) );
			?>

			<section id="products-comingsoon">
                <div class="container">
                    <?php
                        if ( false !== strpos( $shortcode_content, 'product' ) ) {
                            echo $shortcode_content;
                        } else {
                            echo 'no results';
                        }
			        ?>
                </div>
            </section><?php
		}
	}
}

if ( ! function_exists( 'featured_products2' ) ) {

	function featured_products2( $args ) {
		if ( storefront_is_woocommerce_activated() ) {

		    $args = apply_filters( 'storefront_recent_products_args', array(
		        'limit'    => -1,
				'category' => 'featured'
			) );

			$shortcode_content = storefront_do_shortcode( 'recent_products', apply_filters( 'storefront_recent_products_shortcode_args', array(
				'per_page' => intval( $args['limit'] ),
			    'category' => $args['category'],
			) ) );

			?>

			<section id="products-featured">
                <div class="container">
                    <?php
                        if ( false !== strpos( $shortcode_content, 'product' ) ) {
                            echo $shortcode_content;
                        } else {
                            echo 'no results';
                        }
			        ?>
                </div>
            </section><?php
		}
	}
}

if ( ! function_exists( 'product_list' ) ) {
	/**
	 * Display Featured Products
	 * Hooked into the `homepage` action in the homepage template
	 *
	 * @since  1.0.0
	 * @param array $args the product section args.
	 * @return void
	 */
	function product_list( $args ) {

		if ( storefront_is_woocommerce_activated() ) {

			$args = apply_filters( 'storefront_featured_products_args', array(
				'orderby' => 'date',
				'order'   => 'desc'
			) );

			$shortcode_content = storefront_do_shortcode( 'featured_products', apply_filters( 'storefront_featured_products_shortcode_args', array(
				'orderby'  => esc_attr( $args['orderby'] ),
				'order'    => esc_attr( $args['order'] ),
			) ) );

			/**
			 * Only display the section if the shortcode returns products
			 */
			echo '<div class="container">';

			if ( false !== strpos( $shortcode_content, 'product' ) ) {

				echo $shortcode_content;

			}

			echo '</div>';
		}
	}
}

if ( ! function_exists( 'storefront_popular_products' ) ) {
	/**
	 * Display Popular Products
	 * Hooked into the `homepage` action in the homepage template
	 *
	 * @since  1.0.0
	 * @param array $args the product section args.
	 * @return void
	 */
	function storefront_popular_products( $args ) {

		if ( storefront_is_woocommerce_activated() ) {

			$args = apply_filters( 'storefront_popular_products_args', array(
				'limit'   => 4,
				'columns' => 4,
				'title'   => __( 'Fan Favorites', 'storefront' ),
			) );

			$shortcode_content = storefront_do_shortcode( 'top_rated_products', apply_filters( 'storefront_popular_products_shortcode_args', array(
				'per_page' => intval( $args['limit'] ),
				'columns'  => intval( $args['columns'] ),
			) ) );

			/**
			 * Only display the section if the shortcode returns products
			 */
			if ( false !== strpos( $shortcode_content, 'product' ) ) {

				echo '<section class="storefront-product-section storefront-popular-products" aria-label="' . esc_attr__( 'Popular Products', 'storefront' ) . '">';

				do_action( 'storefront_homepage_before_popular_products' );

				echo '<h2 class="section-title">' . wp_kses_post( $args['title'] ) . '</h2>';

				do_action( 'storefront_homepage_after_popular_products_title' );

				echo $shortcode_content;

				do_action( 'storefront_homepage_after_popular_products' );

				echo '</section>';

			}
		}
	}
}

if ( ! function_exists( 'storefront_on_sale_products' ) ) {
	/**
	 * Display On Sale Products
	 * Hooked into the `homepage` action in the homepage template
	 *
	 * @param array $args the product section args.
	 * @since  1.0.0
	 * @return void
	 */
	function storefront_on_sale_products( $args ) {

		if ( storefront_is_woocommerce_activated() ) {

			$args = apply_filters( 'storefront_on_sale_products_args', array(
				'limit'   => 4,
				'columns' => 4,
				'title'   => __( 'On Sale', 'storefront' ),
			) );

			$shortcode_content = storefront_do_shortcode( 'sale_products', apply_filters( 'storefront_on_sale_products_shortcode_args', array(
				'per_page' => intval( $args['limit'] ),
				'columns'  => intval( $args['columns'] ),
			) ) );

			/**
			 * Only display the section if the shortcode returns products
			 */
			if ( false !== strpos( $shortcode_content, 'product' ) ) {

				echo '<section class="storefront-product-section storefront-on-sale-products" aria-label="' . esc_attr__( 'On Sale Products', 'storefront' ) . '">';

				do_action( 'storefront_homepage_before_on_sale_products' );

				echo '<h2 class="section-title">' . wp_kses_post( $args['title'] ) . '</h2>';

				do_action( 'storefront_homepage_after_on_sale_products_title' );

				echo $shortcode_content;

				do_action( 'storefront_homepage_after_on_sale_products' );

				echo '</section>';

			}
		}
	}
}

if ( ! function_exists( 'storefront_best_selling_products' ) ) {
	/**
	 * Display Best Selling Products
	 * Hooked into the `homepage` action in the homepage template
	 *
	 * @since 2.0.0
	 * @param array $args the product section args.
	 * @return void
	 */
	function storefront_best_selling_products( $args ) {
		if ( storefront_is_woocommerce_activated() ) {

			$args = apply_filters( 'storefront_best_selling_products_args', array(
				'limit'   => 4,
				'columns' => 4,
				'title'	  => esc_attr__( 'Best Sellers', 'storefront' ),
			) );

			$shortcode_content = storefront_do_shortcode( 'best_selling_products', apply_filters( 'storefront_best_selling_products_shortcode_args', array(
				'per_page' => intval( $args['limit'] ),
				'columns'  => intval( $args['columns'] ),
			) ) );

			/**
			 * Only display the section if the shortcode returns products
			 */
			if ( false !== strpos( $shortcode_content, 'product' ) ) {

				echo '<section class="storefront-product-section storefront-best-selling-products" aria-label="' . esc_attr__( 'Best Selling Products', 'storefront' ) . '">';

				do_action( 'storefront_homepage_before_best_selling_products' );

				echo '<h2 class="section-title">' . wp_kses_post( $args['title'] ) . '</h2>';

				do_action( 'storefront_homepage_after_best_selling_products_title' );

				echo $shortcode_content;

				do_action( 'storefront_homepage_after_best_selling_products' );

				echo '</section>';

			}
		}
	}
}

if ( ! function_exists( 'homepage_content' ) ) {
	/**
	 * Display homepage content
	 * Hooked into the `homepage` action in the homepage template
	 *
	 * @since  1.0.0
	 * @return  void
	 */
	function homepage_content() {
	    echo '<section class="hero">';
		while ( have_posts() ) {
			the_post();

			get_template_part( 'content', 'homepage' );

		} // end of the loop.
        echo '</section>';
	}
}

if ( ! function_exists( 'storefront_social_icons' ) ) {
	/**
	 * Display social icons
	 * If the subscribe and connect plugin is active, display the icons.
	 *
	 * @link http://wordpress.org/plugins/subscribe-and-connect/
	 * @since 1.0.0
	 */
	function storefront_social_icons() {
		if ( class_exists( 'Subscribe_And_Connect' ) ) {
			echo '<div class="subscribe-and-connect-connect">';
			subscribe_and_connect_connect();
			echo '</div>';
		}
	}
}

if ( ! function_exists( 'storefront_get_sidebar' ) ) {
	/**
	 * Display storefront sidebar
	 *
	 * @uses get_sidebar()
	 * @since 1.0.0
	 */
	function storefront_get_sidebar() {
		get_sidebar();
	}
}

if ( ! function_exists( 'storefront_post_thumbnail' ) ) {
	/**
	 * Display post thumbnail
	 *
	 * @var $size thumbnail size. thumbnail|medium|large|full|$custom
	 * @uses has_post_thumbnail()
	 * @uses the_post_thumbnail
	 * @param string $size the post thumbnail size.
	 * @since 1.5.0
	 */
	function storefront_post_thumbnail( $size = 'thumbnail' ) {
		if ( has_post_thumbnail() ) {
			the_post_thumbnail( $size );
		}
	}
}

if ( ! function_exists( 'tefit_wc_social_list')) {
    /**
     * Кнопки социальных сетей
     */
	function tefit_wc_social_list() {
		global $post;
		?><div class="social-buttons">

		<?php if(get_post_meta($post->ID, 'bitcointalk_link', true)) { ?>
			<a class="social-button u-bitcoin" href="<?php echo get_post_meta($post->ID, 'bitcointalk_link', true) ?>" target="_blank" rel="nofollow"></a>
		<?php } else { ?>
			<span class="social-button u-bitcoin is-disabled"></span>
		<?php } ?>

		<?php if(get_post_meta($post->ID, 'reddit_link', true)) { ?>
			<a class="social-button u-reddit" href="<?php echo get_post_meta($post->ID, 'reddit_link', true) ?>" target="_blank" rel="nofollow"></a>
		<?php } else { ?>
			<span class="social-button u-reddit is-disabled"></span>
		<?php } ?>

		<?php if(get_post_meta($post->ID, 'github_link', true)) { ?>
			<a class="social-button u-github" href="<?php echo get_post_meta($post->ID, 'github_link', true) ?>" target="_blank" rel="nofollow"></a>
		<?php } else { ?>
			<span class="social-button u-github is-disabled"></span>
		<?php } ?>

		<?php if(get_post_meta($post->ID, 'facebook_link', true)) { ?>
			<a class="social-button u-facebook" href="<?php echo get_post_meta($post->ID, 'facebook_link', true) ?>" target="_blank" rel="nofollow"></a>
		<?php } else { ?>
			<span class="social-button u-facebook is-disabled"></span>
		<?php } ?>

		<?php if(get_post_meta($post->ID, 'twitter_link', true)) { ?>
			<a class="social-button u-twitter" href="<?php echo get_post_meta($post->ID, 'twitter_link', true) ?>" target="_blank" rel="nofollow"></a>
		<?php } else { ?>
			<span class="social-button u-twitter is-disabled"></span>
		<?php } ?>

		<?php if(get_post_meta($post->ID, 'telegram_link', true)) { ?>
			<a class="social-button u-telegram" href="<?php echo get_post_meta($post->ID, 'telegram_link', true) ?>" target="_blank" rel="nofollow"></a>
		<?php } else { ?>
			<span class="social-button u-telegram is-disabled"></span>
		<?php } ?>

		<?php if(get_post_meta($post->ID, 'website_link', true)) { ?>
			<a class="social-button u-www" href="<?php echo get_post_meta($post->ID, 'website_link', true) ?>" target="_blank" rel="nofollow"></a>
		<?php } else { ?>
			<span class="social-button u-www is-disabled"></span>
		<?php } ?>
		</div>
		<a class="product__read-more" href="<?php echo get_the_permalink() ?>">Read More</a><?php
	}
}

if (! function_exists('single_page_short_description')) {
	function single_page_short_description() {
		global $product;

		// Ensure visibility
		if ( empty( $product ) || ! $product->is_visible() ) {
			return;
		}

//		echo($product->get_short_description());
	}
}