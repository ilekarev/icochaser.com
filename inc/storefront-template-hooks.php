<?php
/**
 * Storefront hooks
 *
 * @package storefront
 */

/**
 * General
 *
 * @see  storefront_header_widget_region()
 * @see  storefront_get_sidebar()
 */
add_action( 'storefront_before_content', 'storefront_header_widget_region', 10 );
//add_action( 'storefront_sidebar',        'storefront_get_sidebar',          10 );

/**
 * Header
 *
 * @see  storefront_secondary_navigation()
 * @see  site_branding()
 * @see  primary_navigation()
 */
add_action( 'storefront_header', 'site_branding',                    20 );
add_action( 'storefront_header', 'storefront_secondary_navigation',  30 );
add_action( 'storefront_header', 'primary_navigation',               50 );

/**
 * Footer
 *
 * @see  storefront_footer_widgets()
 * @see  storefront_credit()
 */
add_action( 'storefront_footer', 'storefront_footer_widgets', 10 );
add_action( 'storefront_footer', 'storefront_credit',         20 );

/**
 * Homepage
 *
 * @see  homepage_content()
 * @see  storefront_recent_products()
 * @see  featured_products()
 * @see  storefront_popular_products()
 * @see  storefront_on_sale_products()
 * @see  storefront_best_selling_products()
 */
add_action( 'homepage', 'homepage_content',          	10 );
add_action( 'homepage', 'homepage_featured_products',   20 );
add_action( 'homepage', 'limit_ongoing_products',    	25 );
add_action( 'homepage', 'limit_comingsoon_products', 	30 );

/**
 * ICO List
 */
add_action( 'icolist', 'comingsoon_products',  12 );       // coming-soon
add_action( 'icolist', 'featured_products2',   11 );       // featured
add_action( 'icolist', 'featured_product_new', 5 );        // all
add_action( 'icolist', 'ongoing_products',     13 );       // ongoing

/**
 * Bounties
 */
add_action( 'bounties', 'bounties_hot_products',  10 );
add_action( 'bounties', 'bounties_products',      20 );

/**
 * Posts
 *
 * @see  storefront_post_header()
 * @see  storefront_post_meta()
 * @see  storefront_post_content()
 * @see  storefront_paging_nav()
 * @see  storefront_single_post_header()
 * @see  storefront_post_nav()
 * @see  storefront_display_comments()
 */
add_action( 'storefront_loop_post',           'storefront_post_header',          10 );
add_action( 'storefront_loop_post',           'storefront_post_meta',            20 );
add_action( 'storefront_loop_post',           'storefront_post_content',         30 );
add_action( 'storefront_loop_after',          'storefront_paging_nav',           10 );
add_action( 'storefront_single_post',         'storefront_post_header',          10 );
add_action( 'storefront_single_post',         'storefront_post_meta',            20 );
add_action( 'storefront_single_post',         'storefront_post_content',         30 );
add_action( 'storefront_single_post_bottom',  'storefront_post_nav',             10 );
add_action( 'storefront_single_post_bottom',  'storefront_display_comments',     20 );
add_action( 'storefront_post_content_before', 'storefront_post_thumbnail',       10 );

/**
 * Pages
 *
 * @see  storefront_page_header()
 * @see  storefront_page_content()
 * @see  storefront_display_comments()
 */
add_action( 'storefront_page',       'storefront_page_header',          10 );
add_action( 'storefront_page',       'storefront_page_content',         20 );
add_action( 'storefront_page_after', 'storefront_display_comments',     10 );

add_action( 'storefront_homepage',       'storefront_homepage_header',      10 );
add_action( 'storefront_homepage',       'storefront_page_content',         20 );