<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package storefront
 */

?>

	<?php do_action( 'storefront_before_footer' ); ?>
	<footer class="footer" role="contentinfo">
		<div class="container">
			<div class="bottom-line">
				<div class="column">
					<a class="footer__logo" href="<?php echo( esc_url( home_url( '/' ) ) ); ?>"></a>
				</div>
				<div class="column">
					<a class="footer__page-link" href="<?php echo( esc_url( home_url( '/' ) ) ); ?>ico-list">ICO list</a>
					<a class="footer__page-link" href="https://medium.com/icochaserblog" target="_blank" rel="nofollow">Blog</a>
					<a class="footer__page-link" href="<?php echo( esc_url( home_url( '/' ) ) ); ?>bounties">Bounties</a>
				</div>
				<div class="column">
					<span class="footer__page-link u-nolink">Contact us: <a class="footer__mail-link" href="mailto:info@icochaser.com">info@icochaser.com</a></span>
					<a class="footer__page-link" href="<?php echo( esc_url( home_url( '/' ) ) ); ?>user-warranties-and-disclaimer">Disclaimer</a>
				</div>
				<div class="column">
					<div class="footer__subscribe-wrapper">
                        <?php echo do_shortcode('[mc4wp_form id="234"]'); ?>
                    </div>
                    <div class="fa-socials">
						<a class="fa-social-button" href="https://t.me/icochaserbountymanagement" target="_blank" rel="nofollow"><i class="fa fa-telegram" aria-hidden="true"></i></a>
                        <a class="fa-social-button" href="https://twitter.com/Ico_Chaser" target="_blank" rel="nofollow"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                    </div>

				</div>
			</div>
			<?php
			/**
			 * Functions hooked in to storefront_footer action
			 *
			 * @hooked storefront_footer_widgets - 10
			 * @hooked storefront_credit         - 20
			 */
//			do_action( 'storefront_footer' ); ?>

		</div><!-- .col-full -->
	</footer><!-- #colophon -->

<link rel="stylesheet" href="<?php echo(get_stylesheet_directory_uri() . '/assets/css/owl-carousel/owl.carousel.min.css') ?>">
<link rel="stylesheet" href="<?php echo(get_stylesheet_directory_uri() . '/assets/css/owl-carousel/owl.theme.default.min.css') ?>">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="<?php echo(get_stylesheet_directory_uri() . '/assets/js/owl-carousel/owl.carousel.min.js') ?>"></script>
<script>
    jQuery(document).ready(function(){
        jQuery('section.featured-products .products').owlCarousel({
            loop:true,
            margin:10,
            nav:false,
            autoplay: true,
            autoplayTimeout: 7000,
            responsive:{
                0:{
                    items:1
                },
                800:{
                    items:2
                },
                1170:{
                    items:3
                }
            }
        });
    });
</script>

	<?php do_action( 'storefront_after_footer' ); ?>

<?php wp_footer(); ?>

</body>
</html>
