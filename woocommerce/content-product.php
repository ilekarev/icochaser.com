<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

global $product;

// Ensure visibility
if ( empty( $product ) || ! $product->is_visible() ) {
    return;
}
?>

<div <?php post_class(); ?>> <?php

    if (is_page_template( 'template-bounty.php' )) {
        if (has_term(array('bounties', 'bounties-hot'), 'product_cat')) {

            global $post; ?>

            <a class="bounty__link" href="<?php echo get_post_meta($post->ID, 'bounty_link', true) ?>" target="_blank" rel="nofollow"><?php

            if (has_term('bounties-hot', 'product_cat')) { ?>
                <img class="bounty__hot-sticker"
                     src="<?php echo(get_stylesheet_directory_uri() . '/assets/images/hot.png') ?>" alt="HOT">
                <div class="bounty__image-wrapper">
                <img class="bounty__image" src="<?php echo get_post_meta($post->ID, 'bounty_hot', true) ?>">
                </div><?php

            } else { ?>

                <span class="bounty__title"><?php echo get_the_title() ?></span>
                <span class="bounty__short-description"><?php echo($product->get_short_description()); ?></span><?php

            } ?>

            </a><?php
        }
    } else {

        /**
         * woocommerce_before_shop_loop_item hook.
         *
         * @hooked woocommerce_template_loop_product_link_open - 10
         */
        do_action('woocommerce_before_shop_loop_item');

        /**
         * woocommerce_before_shop_loop_item_title hook.
         *
         * @hooked tefit_wc_image_wrapper_open, 5
         * @hooked woocommerce_show_product_loop_sale_flash - 10
         * @hooked woocommerce_template_loop_product_thumbnail - 10
         * @hooked tefit_wc_image_wrapper_open, 15
         */
        do_action('woocommerce_before_shop_loop_item_title');

        /**
         * woocommerce_shop_loop_item_title hook.
         *
         * @hooked woocommerce_template_loop_product_title - 10
         */
        do_action('woocommerce_shop_loop_item_title');
        ?>

        <div class="product__information">

            <div class="product__description-text"><?php echo($product->get_short_description()); ?></div>

            <div class="product__information-row">
                <?php
                /**
                 * woocommerce_after_shop_loop_item_title hook.
                 *
                 * @hooked wc_product_dates_item - 5
                 * @hooked wc_product_whiteboard_item_start - 6 remove
                 * @hooked wc_token_price - 10
                 * @hooked wc_product_whiteboard_item_end - 40
                 */
                do_action('woocommerce_after_shop_loop_item_title');
                ?>
            </div>
        </div>
        <?php
        /**
         * woocommerce_after_shop_loop_item hook.
         *
         * @hooked woocommerce_template_loop_product_link_close - 15
         * @hooked woocommerce_template_loop_add_to_cart - 10
         */
        do_action('woocommerce_after_shop_loop_item');
    } ?>

</div>