<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<?php
/**
 * woocommerce_before_single_product hook.
 *
 * @hooked wc_print_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form();
	return;
}

global $post;

?>

<div id="product-<?php the_ID(); ?>" <?php post_class(); ?>>
	<section class="section">
		<div class="container">

			<div class="columns is-centered">
				<div class="column">
					<div class="single-product__image-wrapper">
						<?php echo woocommerce_get_product_thumbnail(); ?>
					</div>
				</div>
			</div>

			<div class="columns is-multiline">
				<div class="column">
					<div class="single-product__media-wrapper">
					<?php
					if (get_post_meta($post->ID, 'videolink', true)) { ?>
						<iframe width="300" height="169" src="https://www.youtube.com/embed/<?php echo get_post_meta($post->ID, 'videolink', true); ?>" frameborder="0" allowfullscreen></iframe>
					<?php
					} else {
						$post_thumbnail_id = get_post_thumbnail_id($post->ID);
						$attributes = array(
							'title' => get_post_field('post_title', $post_thumbnail_id),
						);
						if (has_post_thumbnail()) {
							$html .= get_the_post_thumbnail($post->ID, 'shop_single', $attributes);
						} else {
							$html = '';
						} ?>

						<div class="single-product__image-wrapper u-main-img">
							<?php
							echo apply_filters('woocommerce_single_product_image_thumbnail_html', $html, get_post_thumbnail_id($post->ID));
							?>
						</div><?php
						do_action('woocommerce_product_thumbnails');
					} ?>
					</div>

					<div class="single-product__details">
						<h3 class="featured-products__title">ICO Details</h3>
						<div class="single-product__details-grid">
							<div class="is-row">Symbol</div>
							<div class="is-row"><?php echo get_post_meta($post->ID, 'token_symbol', true); ?></div>
							<div class="is-row">Start Date</div>
							<div class="is-row"><?php echo get_post_meta($post->ID, 'date_start', true); ?></div>
							<div class="is-row">End Date</div>
							<div class="is-row"><?php echo get_post_meta($post->ID, 'date_end', true); ?></div>
							<div class="is-row">Base</div>
							<div class="is-row"><?php echo get_post_meta($post->ID, 'base_currency', true); ?></div>
							<div class="is-row">Token Total Supply</div>
							<div class="is-row"><?php echo get_post_meta($post->ID, 'total_token_supply', true); ?></div>
							<div class="is-row">Token Price</div>
							<div class="is-row"><?php echo get_post_meta($post->ID, 'token_price', true); ?></div>
						</div>
					</div>
				</div>

				<div class="column">
						<?php
						/**
						 * woocommerce_single_product_summary hook.
						 *
						 * @hooked woocommerce_template_single_title - 5
						 * @hooked single_page_short_description - 7
						 * @hooked woocommerce_template_single_rating - 10
						 * @hooked woocommerce_template_single_price - 10
						 * @hooked woocommerce_template_single_excerpt - 6
						 * @hooked woocommerce_template_single_add_to_cart - 30
						 * @hooked woocommerce_template_single_meta - 40
						 * @hooked woocommerce_template_single_sharing - 50
						 * @hooked WC_Structured_Data::generate_product_data() - 60
						 */
						do_action( 'woocommerce_single_product_summary' );
						?>


					<?php
					/**
					 * woocommerce_after_single_product_summary hook.
					 *
					 * @hooked woocommerce_output_product_data_tabs - 10
					 * @hooked woocommerce_upsell_display - 15
					 * @hooked woocommerce_output_related_products - 20
					 */
					do_action( 'woocommerce_after_single_product_summary' );
					?>

					<?php // Здесь кнопочки Whitepaper (pdf - link), Get Tokens (token sale link) ?>

					<div class="single-product__buttons-row">
						<a class="btn" href="<?php echo get_post_meta($post->ID, 'pdflink', true); ?>" target="_blank" rel="nofollow">Whitepaper</a>
						<a class="btn" href="<?php echo get_post_meta($post->ID, 'token_sale_link', true); ?>" target="_blank" rel="nofollow">Get Tokens</a>
					</div>

				</div>
			</div>
		</div>
	</section>

</div><!-- #product-<?php the_ID(); ?> -->

<?php do_action( 'woocommerce_after_single_product' ); ?>
