<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Замена функции
 *
 * Вывод только описания товара без табов
 */
?>
<div class="single-product__description">
	<?php the_content() ?>
</div>
