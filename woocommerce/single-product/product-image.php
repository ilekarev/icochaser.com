<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $post;
$post_thumbnail_id = get_post_thumbnail_id( $post->ID );
?>
	<figure class="full-logo">
		<?php
		$attributes = array(
			'title' => get_post_field( 'post_title', $post_thumbnail_id ),
		);

		if ( has_post_thumbnail() ) {
			$html .= get_the_post_thumbnail( $post->ID, 'shop_single', $attributes );
		} else {
			$html  = '';
		}

		echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', $html, get_post_thumbnail_id( $post->ID ) );

		do_action( 'woocommerce_product_thumbnails' );
		?>
	</figure>