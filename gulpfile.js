const
    gulp = require('gulp'),
    sass = require('gulp-sass'),
    postcss = require('gulp-postcss'),
    sourcemaps = require('gulp-sourcemaps'),
    autoprefixer = require('autoprefixer'),
    bourbon = require('node-bourbon');

//style paths
const
    sassFiles = '*.scss',
    cssDest = '.';

gulp.task('sass', function() {
    let processors = [
        autoprefixer({browsers: ['> 1%']})
    ];

    gulp.src(sassFiles)
        .pipe(sourcemaps.init())
        .pipe(sass({
            includePaths: [
                bourbon.includePaths
            ]
        }).on('error', sass.logError))
        .pipe(postcss(processors))
        .pipe(sourcemaps.write(cssDest))
        .pipe(gulp.dest(cssDest))
});

gulp.task('watch', ['sass'], function() {
    gulp.watch('**/*.scss', ['sass']);
});

gulp.task('default', ['watch']);