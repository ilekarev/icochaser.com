<?php
/**
 * Storefront engine room
 *
 * @package storefront
 */

/**
 * Assign the Storefront version to a var
 */
$theme              = wp_get_theme( 'storefront' );
$storefront_version = $theme['Version'];

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 980; /* pixels */
}

$storefront = (object) array(
	'version' => $storefront_version,

	/**
	 * Initialize all the things.
	 */
	'main'       => require 'inc/class-storefront.php',
	'customizer' => require 'inc/customizer/class-storefront-customizer.php',
);

require 'inc/storefront-functions.php';
require 'inc/storefront-template-hooks.php';
require 'inc/storefront-template-functions.php';

if ( class_exists( 'Jetpack' ) ) {
	$storefront->jetpack = require 'inc/jetpack/class-storefront-jetpack.php';
}

if ( storefront_is_woocommerce_activated() ) {
	$storefront->woocommerce = require 'inc/woocommerce/class-storefront-woocommerce.php';

	require 'inc/woocommerce/storefront-woocommerce-template-hooks.php';
	require 'inc/woocommerce/storefront-woocommerce-template-functions.php';
}

if ( is_admin() ) {
	$storefront->admin = require 'inc/admin/class-storefront-admin.php';

	require 'inc/admin/class-storefront-plugin-install.php';
}

/**
 * NUX
 * Only load if wp version is 4.7.3 or above because of this issue;
 * https://core.trac.wordpress.org/ticket/39610?cversion=1&cnum_hist=2
 */
if ( version_compare( get_bloginfo( 'version' ), '4.7.3', '>=' ) && ( is_admin() || is_customize_preview() ) ) {
	require 'inc/nux/class-storefront-nux-admin.php';
	require 'inc/nux/class-storefront-nux-guided-tour.php';

	if ( defined( 'WC_VERSION' ) && version_compare( WC_VERSION, '3.0.0', '>=' ) ) {
		require 'inc/nux/class-storefront-nux-starter-content.php';
	}
}


/*
 * Add Custom Field. Добавить поля на страницу редактирования товара
 */
function add_cf($id, $label) {
	woocommerce_wp_text_input( array(
		'id'    => $id,
		'label' => __( $label, 'woocommerce' )
	) );
}

/*
 * Save Data Product. Сохранить данные с полей
 */
function save_dp($post_id, $input_id) {
	$custom_field_value = isset( $_POST[$input_id] ) ? $_POST[$input_id] : '';

	$product = wc_get_product( $post_id );
	$product->update_meta_data( $input_id, $custom_field_value );
	$product->save();
}

add_action( 'woocommerce_product_options_pricing', 'wc_icotagline_product_field' );
add_action( 'woocommerce_product_options_pricing', 'wc_videolink_product_field' );
add_action( 'woocommerce_product_options_pricing', 'wc_pdflink_product_field' );
add_action( 'woocommerce_product_options_pricing', 'wc_token_symbol_product_field' );
add_action( 'woocommerce_product_options_pricing', 'wc_total_token_supply_product_field' );
add_action( 'woocommerce_product_options_pricing', 'wc_token_price_product_field' );
add_action( 'woocommerce_product_options_pricing', 'wc_base_currency_product_field' );
add_action( 'woocommerce_product_options_pricing', 'wc_contact_email_product_field' );
add_action( 'woocommerce_product_options_pricing', 'wc_contact_name_product_field' );
add_action( 'woocommerce_product_options_pricing', 'wc_token_sale_link_product_field' );
add_action( 'woocommerce_product_options_pricing', 'wc_date_start_product_field' );
add_action( 'woocommerce_product_options_pricing', 'wc_date_end_product_field' );
add_action( 'woocommerce_product_options_pricing', 'wc_twitter_link_product_field' );
add_action( 'woocommerce_product_options_pricing', 'wc_telegram_link_product_field' );
add_action( 'woocommerce_product_options_pricing', 'wc_reddit_link_product_field' );
add_action( 'woocommerce_product_options_pricing', 'wc_bitcointalk_link_product_field' );
add_action( 'woocommerce_product_options_pricing', 'wc_github_link_product_field' );
add_action( 'woocommerce_product_options_pricing', 'wc_facebook_link_product_field' );
add_action( 'woocommerce_product_options_pricing', 'wc_website_link_product_field' );
add_action( 'woocommerce_product_options_pricing', 'wc_bounty_link_product_field' );
add_action( 'woocommerce_product_options_pricing', 'wc_bounty_hot_image_field' );

function wc_icotagline_product_field()         { add_cf('icotagline', 'ICO Tag Line');               }
function wc_videolink_product_field()          { add_cf('videolink', 'YouTube Video ID');            }
function wc_pdflink_product_field()            { add_cf('pdflink', 'PDF Link');                      }
function wc_token_symbol_product_field()       { add_cf('token_symbol', 'Token Symbol');             }
function wc_total_token_supply_product_field() { add_cf('total_token_supply', 'Total Token Supply'); }
function wc_token_price_product_field()        { add_cf('token_price', 'Token Price');               }
function wc_base_currency_product_field()      { add_cf('base_currency', 'Base Currency');           }
function wc_contact_email_product_field()      { add_cf('contact_email', 'Contact Email');           }
function wc_contact_name_product_field()       { add_cf('contact_name', 'Contact/Founder Name');     }
function wc_token_sale_link_product_field()    { add_cf('token_sale_link', 'Token Sale Link');       }
function wc_date_start_product_field()         { add_cf('date_start', 'Start Date');                 }
function wc_date_end_product_field()           { add_cf('date_end', 'End Date');                     }
function wc_twitter_link_product_field()       { add_cf('twitter_link', 'Twitter');                  }
function wc_telegram_link_product_field()      { add_cf('telegram_link', 'Telegram');                }
function wc_reddit_link_product_field()        { add_cf('reddit_link', 'Reddit');                    }
function wc_bitcointalk_link_product_field()   { add_cf('bitcointalk_link', 'BitcoinTalk');          }
function wc_github_link_product_field()        { add_cf('github_link', 'Github');                    }
function wc_facebook_link_product_field()      { add_cf('facebook_link', 'Facebook');                }
function wc_website_link_product_field()       { add_cf('website_link', 'WebSite/ICO');              }
function wc_bounty_link_product_field()        { add_cf('bounty_link', 'Bounty');                    }
function wc_bounty_hot_image_field()           { add_cf('bounty_hot', 'Bounty HOT (image)');         }
//=======================
add_action( 'woocommerce_process_product_meta', 'wc_icotagline_save_product' );
add_action( 'woocommerce_process_product_meta', 'wc_videolink_save_product' );
add_action( 'woocommerce_process_product_meta', 'wc_pdflink_save_product' );
add_action( 'woocommerce_process_product_meta', 'wc_token_symbol_save_product' );
add_action( 'woocommerce_process_product_meta', 'wc_total_token_supply_save_product' );
add_action( 'woocommerce_process_product_meta', 'wc_token_price_save_product' );
add_action( 'woocommerce_process_product_meta', 'wc_base_currency_save_product' );
add_action( 'woocommerce_process_product_meta', 'wc_contact_email_save_product' );
add_action( 'woocommerce_process_product_meta', 'wc_contact_name_save_product' );
add_action( 'woocommerce_process_product_meta', 'wc_token_sale_link_save_product' );
add_action( 'woocommerce_process_product_meta', 'wc_date_start_save_product' );
add_action( 'woocommerce_process_product_meta', 'wc_date_end_save_product' );
add_action( 'woocommerce_process_product_meta', 'wc_twitter_link_save_product' );
add_action( 'woocommerce_process_product_meta', 'wc_telegram_link_save_product' );
add_action( 'woocommerce_process_product_meta', 'wc_reddit_link_save_product' );
add_action( 'woocommerce_process_product_meta', 'wc_bitcointalk_link_save_product' );
add_action( 'woocommerce_process_product_meta', 'wc_github_link_save_product' );
add_action( 'woocommerce_process_product_meta', 'wc_facebook_link_save_product' );
add_action( 'woocommerce_process_product_meta', 'wc_website_link_save_product' );
add_action( 'woocommerce_process_product_meta', 'wc_bounty_link_save_product' );
add_action( 'woocommerce_process_product_meta', 'wc_bounty_hot_image_save' );

function wc_icotagline_save_product($post_id)         { save_dp($post_id, 'icotagline');         }
function wc_videolink_save_product($post_id)          { save_dp($post_id, 'videolink');          }
function wc_pdflink_save_product($post_id)            { save_dp($post_id, 'pdflink');            }
function wc_token_symbol_save_product($post_id)       { save_dp($post_id, 'token_symbol');       }
function wc_total_token_supply_save_product($post_id) { save_dp($post_id, 'total_token_supply'); }
function wc_token_price_save_product($post_id)        { save_dp($post_id, 'token_price');        }
function wc_base_currency_save_product($post_id)      { save_dp($post_id, 'base_currency');      }
function wc_contact_email_save_product($post_id)      { save_dp($post_id, 'contact_email');      }
function wc_contact_name_save_product($post_id)       { save_dp($post_id, 'contact_name');       }
function wc_token_sale_link_save_product($post_id)    { save_dp($post_id, 'token_sale_link');    }
function wc_date_start_save_product($post_id)         { save_dp($post_id, 'date_start');         }
function wc_date_end_save_product($post_id)           { save_dp($post_id, 'date_end');           }
function wc_twitter_link_save_product($post_id)       { save_dp($post_id, 'twitter_link');       }
function wc_telegram_link_save_product($post_id)      { save_dp($post_id, 'telegram_link');      }
function wc_reddit_link_save_product($post_id)        { save_dp($post_id, 'reddit_link');        }
function wc_bitcointalk_link_save_product($post_id)   { save_dp($post_id, 'bitcointalk_link');   }
function wc_github_link_save_product($post_id)        { save_dp($post_id, 'github_link');        }
function wc_facebook_link_save_product($post_id)      { save_dp($post_id, 'facebook_link');      }
function wc_website_link_save_product($post_id)       { save_dp($post_id, 'website_link');       }
function wc_bounty_link_save_product($post_id)        { save_dp($post_id, 'bounty_link');        }
function wc_bounty_hot_image_save($post_id)           { save_dp($post_id, 'bounty_hot');         }
//=======================

function wc_product_dates_item() {
    global $post;
    ?>
    <div class="product__column u-no-resize u-left-column">
        <?php if (get_post_meta($post->ID, 'date_start', true)) { ?>
            <div class="product__date-start"><span>Start Date </span><?php echo get_post_meta($post->ID, 'date_start', true) ?></div>
        <?php } ?>
        <?php if (get_post_meta($post->ID, 'date_end', true)) { ?>
            <div class="product__date-end"><span>End Date </span><?php echo get_post_meta($post->ID, 'date_end', true) ?></div>
        <?php } ?>
    </div>
    <?php
}

function wc_token_price() {
	global $post;

	if (get_post_meta($post->ID, 'token_price', true)) { ?>
		<div class="product__token-price">Token Price <?php echo get_post_meta($post->ID, 'token_price', true) ?></div>
	<?php }
}

function wc_product_whitepaper_item_start() { ?>
    <div class="product__column"><?php
}

function wc_product_whitepaper_item_end() {
    global $post;

    if (get_post_meta($post->ID, 'bounty_link', true)) { ?>
<!--            <a class="product__bounty_link" href="--><?php //echo get_post_meta($post->ID, 'bounty_link', true) ?><!--">Whitepaper</a>-->
        <?php } ?>
    </div><?php
}

function tefit_wc_image_wrapper_open() {
    ?><div class="product__image-wrapper"><?php
}

function tefit_wc_image_wrapper_close() {
    ?></div><?php
}
